if (navigator.geolocation) {

	function GoogleMapsGeo(position) {

		var geo = [
			[position.coords.latitude, position.coords.longitude],
			[37.4216164, -122.0843257], 
			[48.8583189, 2.2945537], 
			[-33.8583079, 151.215286],
			[-14.2350144, -51.9264816],
			[35.86166, 104.195397],
			[43.647578, -79.375646]
		];

	    var optionsGmaps = {
	    	mapTypeControl: true,
	    	center: new google.maps.LatLng(0, 0),
	    	navigationControlOptions: {style:google.maps.NavigationControlStyle.SMALL},
	    	mapTypeId: google.maps.MapTypeId.ROADMAP,
	    	zoom: 3
	    };

	    var map = new google.maps.Map(document.getElementById("maps"), optionsGmaps);

	    geo.forEach(function(coords){
	    	new google.maps.Marker({
		    	position: new google.maps.LatLng(coords[0], coords[1]),
		    	map: map
	    	});
		});

	}

	navigator.geolocation.getCurrentPosition(GoogleMapsGeo);
}